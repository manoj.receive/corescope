﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransientScopedSingleton.Interface
{
    public interface ITransientService
    {
        Guid GetOperationID();
    }
}
